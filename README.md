# 企业云盘

## 声明：
学生作业，仅供参考

#### 介绍
基于SpringBoot与Hadoop实现的的企业云盘项目后端

查看后端接口：localhost:port/swagger-ui/index.html

### 前端

地址：https://gitee.com/ling717/cloud-disk-front-end
技术栈：Vue3 Element-UI

### hadoop 搭建流程：
https://www.bilibili.com/video/BV1Qp4y1n7EN?p=23&vd_source=37969034c023f6c2ea674e912003d2b0