package edu.cuit.clouddisk.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import edu.cuit.clouddisk.tools.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName : TokenLogoutHandler
 * @Author : YuYun
 * @Date : 2022-12-03 17:53:02
 * @Description ：
 */
@Component
public class TokenLogoutHandler implements LogoutHandler {
    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    @Override
    public void logout(HttpServletRequest httpServletRequest,
                       HttpServletResponse httpServletResponse,
                       Authentication authentication) {
        String token = httpServletRequest.getHeader("token");

        httpServletResponse.setContentType("application/json;charset=utf-8");
        httpServletResponse.setStatus(200);
        PrintWriter writer = null;
        ObjectMapper objectMapper = new ObjectMapper();

        RequestResult<Object> res = null;
        if (!StringUtils.isEmpty(token)) {
            res = new RequestResult<>(200, "注销成功");
            // 解析Token
            String username = tokenManager.getUsernameFromToken(token);
            // 删除username from redis
            redisTemplate.delete(username);
            Object o = redisTemplate.opsForValue().get(username);
        }else{
            res = new RequestResult<>(400, "请求信息中未存在该用户");
        }

        try {
            writer = httpServletResponse.getWriter();
            writer.write(objectMapper.writeValueAsString(res));
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (writer != null){
                writer.close();
            }
        }

    }
}
