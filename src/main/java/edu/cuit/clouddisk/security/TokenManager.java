package edu.cuit.clouddisk.security;

import edu.cuit.clouddisk.tools.utils.TimeUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @ClassName : TokenManager
 * @Author : YuYun
 * @Date : 2022-12-03 17:46:03
 * @Description ：
 */
@Component
@Slf4j
public class TokenManager {
    @Value("${token.securityKey}")
    private String securityKey;

    @Value("${token.exp}")
    private long exp;

    public String createToken(String username){
        return Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis()+exp))
                .setNotBefore(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.HS256, securityKey).compact();
    }

    public String getUsernameFromToken(String token){
        String claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(securityKey) // 设置标识名
                    .parseClaimsJws(token)  //解析token
                    .getBody().getSubject();
        }catch (ExpiredJwtException e){
            log.info("JWT Token 过期异常: " + e);
        }

        return claims;
    }

    public long getExp(String token) {
        Date exp = Jwts.parser().
                setSigningKey(securityKey).
                parseClaimsJws(token).
                getBody().getExpiration();
        return exp.getTime();
    }
}
