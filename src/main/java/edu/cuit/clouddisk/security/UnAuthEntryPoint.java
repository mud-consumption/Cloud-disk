package edu.cuit.clouddisk.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName : UnAuthEntryPoint
 * @Author : YuYun
 * @Date : 2022-12-03 17:47:20
 * @Description ：
 */

@Component
public class UnAuthEntryPoint implements AuthenticationEntryPoint {
    // 认证失败的时候调用
    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setContentType("application/json;charset=utf-8");
        httpServletResponse.setStatus(200);
        PrintWriter writer = httpServletResponse.getWriter();
        RequestResult<Object> res = new RequestResult<>(400, "认证失败");
        ObjectMapper objectMapper = new ObjectMapper();
        writer.write(objectMapper.writeValueAsString(res));
        writer.flush();
        writer.close();
    }
}
