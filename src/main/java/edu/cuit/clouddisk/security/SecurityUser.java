package edu.cuit.clouddisk.security;

import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.tools.utils.StringUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName : SecurityUser
 * @Author : YuYun
 * @Date : 2022-12-03 18:25:29
 * @Description ：
 */
public class SecurityUser implements UserDetails {
    // 当前登录的用户
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getPermissionValueList() {
        return permissionValueList;
    }

    public void setPermissionValueList(List<String> permissionValueList) {
        this.permissionValueList = permissionValueList;
    }

    // 存放权限列表
    private List<String> permissionValueList;

    // 拿到当前用户的权限
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> authorities = new ArrayList<>();
        for (String permissonValue: permissionValueList){
            if (!StringUtils.isEmpty(permissonValue)){
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permissonValue);
                authorities.add(authority);
            }
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPsw();
    }

    @Override
    public String getUsername() {
        return user.getAcc();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
