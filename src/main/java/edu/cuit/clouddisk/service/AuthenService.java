package edu.cuit.clouddisk.service;

/**
 * @ClassName : AuthenService
 * @Author : YuYun
 * @Date : 2022-12-08 02:28:32
 * @Description ：
 */
public interface AuthenService {
    public Boolean authentication(String token, String role);
}
