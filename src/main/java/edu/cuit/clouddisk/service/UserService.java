package edu.cuit.clouddisk.service;

import edu.cuit.clouddisk.dto.User;

import java.util.Date;
import java.util.List;

/**
 * @ClassName : UserService
 * @Author : YuYun
 * @Date : 2022-11-14 12:14:23
 * @Description ：
 */
public interface UserService {

    // 根据acc查询表中的数据
    User getUserByAcc(String acc);

    // 根据用户输入查询用户是否存在
    User getUserByPasswordAndEmail(String password, String email);

    //根据用户输入注册用户
    Integer putUserByUsernameAndPasswordAndEmail(String username, String password, String email);

    // 查询用户
    List<User> getUsers(int currentPage, int pageSize);

    // 查询页数
    Integer getPageCount(int pageSize);

    //删除用户
    Integer delUserById(int id);

    // 查询总条数
    Integer getTotal();

    Integer toAdmin(Integer userId);

    // 更新密码
    Integer updatePswByAcc(String email, String password);

    Integer updateInfo(String email, String username, String sex, String phone, Date birth);

    Integer getUserIdByAcc(String userEmail);

    // 模糊查询所有用户
    List<User> getAllUsersByVague(String username, Integer curPage, Integer pageSize);

    // 模糊查询所有用户的总数
    Integer getVagueTotal(String username);
}
