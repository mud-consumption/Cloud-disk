package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.dto.Share;
import edu.cuit.clouddisk.mapper.ShareMapper;
import edu.cuit.clouddisk.service.ShareService;
import edu.cuit.clouddisk.tools.utils.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @ClassName : ShareServiceImpl
 * @Author : YuYun
 * @Date : 2022-12-10 02:19:46
 * @Description ：
 */
@Service
public class ShareServiceImpl implements ShareService {
    @Autowired
    private ShareMapper shareMapper;
    @Override
    @Transactional
    public Integer shareFile(Integer sharerId, Integer fileId, Integer userId) {
        return shareMapper.insertShareInfo(sharerId, fileId, userId, TimeUtils.curTimeToString());
    }

    @Override
    public List<Share> getShareOutInfo(Integer userId, Integer curPage, Integer pageSize) {
        return shareMapper.getShareOutInfo(userId, (curPage - 1)*pageSize, pageSize);
    }

    @Override
    public Integer getShareOutTotal(Integer userId, Integer curPage, Integer pageSize) {
        return shareMapper.getShareOutTotal(userId);
    }

    @Override
    public List<Share> getShareInInfo(Integer userId, Integer curPage, Integer pageSize) {
        return shareMapper.getShareInInfo(userId, (curPage - 1)*pageSize, pageSize);
    }

    @Override
    public Integer getShareInTotal(Integer userId) {
        return shareMapper.getShareInTotal(userId);
    }
}
