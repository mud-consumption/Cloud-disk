package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.mapper.UserMapper;
import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.security.SecurityUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName : UserDetailsServiceImpl
 * @Author : YuYun
 * @Date : 2022-12-03 19:02:16
 * @Description ：
 */

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 从用户名查询到改用户的信息
        User users = userMapper.getUserByacc(username);
        if (users == null){
            throw new UsernameNotFoundException("当前用户不存在");
        }

        SecurityUser securityUser = new SecurityUser();
        securityUser.setUser(users);

        List<String> PermissionValueList = new ArrayList<>();
        if (users.getIsAdmin()){
            PermissionValueList.add("admin");
        }else{
            PermissionValueList.add("user");
        }
        securityUser.setPermissionValueList(PermissionValueList);
        return securityUser;
    }
}
