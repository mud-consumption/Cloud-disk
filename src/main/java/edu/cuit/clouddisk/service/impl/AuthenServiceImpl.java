package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.security.TokenManager;
import edu.cuit.clouddisk.service.AuthenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName : AuthenService
 * @Author : YuYun
 * @Date : 2022-12-08 02:28:40
 * @Description ：
 */
@Service
public class AuthenServiceImpl implements AuthenService {
    @Autowired
    private TokenManager tokenManager;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public Boolean authentication(String token, String role) {
        String username = tokenManager.getUsernameFromToken(token);
        List<String> roles = (List<String>)redisTemplate.opsForValue().get(username);
        if (roles == null){
            return false;
        }
        return roles.contains(role);
    }
}
