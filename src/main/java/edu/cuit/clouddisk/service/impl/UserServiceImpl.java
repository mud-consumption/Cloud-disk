package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.mapper.UserMapper;
import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName : UserServiceImpl
 * @Author : YuYun
 * @Date : 2022-11-14 12:15:49
 * @Description ：
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User getUserByAcc(String acc) {
        return userMapper.getUserByacc(acc);
    }

    @Override
    public User getUserByPasswordAndEmail(String password, String email) {
        return userMapper.getUserByPasswordAndEmail(password, email);
    }

    @Override
    public Integer putUserByUsernameAndPasswordAndEmail(String username, String password, String email) {
        return userMapper.putUserByUsernameAndPasswordAndEmail(username, password, email);
    }

    @Override
    public List<User> getUsers(int currentPage, int pageSize) {
        return userMapper.getUserByPage((currentPage - 1) * pageSize, pageSize);
    }

    @Override
    public Integer getPageCount(int pageSize) {
        return (userMapper.getTotalCount() + pageSize - 1) / pageSize;
    }

    @Override
    public Integer delUserById(int id) {
        return userMapper.delUserById(id);
    }

    @Override
    public Integer getTotal() {
        return userMapper.getTotalCount();
    }

    @Override
    public Integer toAdmin(Integer userId) {
        return userMapper.modifyRole(userId);
    }

    @Override
    public Integer updatePswByAcc(String email, String password) {
        return userMapper.updatePswByAcc(email, password);
    }

    @Override
    public Integer updateInfo(String email, String username, String sex, String phone, Date birth) {
        return userMapper.updateInfoByAcc(email, username, sex, phone, birth);
    }

    @Override
    public Integer getUserIdByAcc(String userEmail) {
        return userMapper.getUserByacc(userEmail).getUserId();
    }

    @Override
    public List<User> getAllUsersByVague(String username, Integer curPage, Integer pageSize) {
        return userMapper.getUserByVagueAndUserName(username, (curPage - 1)*pageSize, pageSize);
    }

    @Override
    public Integer getVagueTotal(String username){
        return userMapper.getTotalOfVague(username);
    }

}
