package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.config.HdfsConfig;
import edu.cuit.clouddisk.mapper.FileMapper;
import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.FileService;
import edu.cuit.clouddisk.tools.wrapper.HDFSWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName : FileServiceImpl
 * @Author : YuYun
 * @Date : 2022-11-14 12:15:29
 * @Description ：
 */
@Service
@Slf4j
public class FileServiceImpl implements FileService {

    @Autowired
    FileMapper fileMapper;

    @Autowired
    HDFSWrapper hdfsWrapper;

    @Autowired
    HdfsConfig hdfsConfig;


    @Override
    public String mkdir(String path) {
        if (hdfsWrapper.fileExists(path)){
            return "fault";
        }

        // 在hdfs中创建文件夹
        boolean mkdir = hdfsWrapper.mkdir(path);
        if (mkdir){
            // 在表中插入文件夹的信息
            File fileInfo = hdfsWrapper.getFileInfo(path);
            Integer integer = fileMapper.insertByFile(fileInfo);
            // 如果是递归目录：查看当前上传的父目录的信息有没有存在数据库里边
            String parent = fileInfo.getParent();
            Integer level = fileInfo.getLevel();
            File fileSql = fileMapper.getFileByUrl(parent);
            while (fileSql == null && level > 1){
                // 表明这个目录不在我的数据库里边，需要插入父目录的信息
                // 从hdfs获取父目录信息
                fileInfo = hdfsWrapper.getFileInfo(parent);
                // 在数据库中插入信息
                fileMapper.insertByFile(fileInfo);

                parent = fileInfo.getParent().substring(hdfsConfig.getNameNodeUrl().length());
                level = fileInfo.getLevel();
                fileSql = fileMapper.getFileByUrl(parent);

            }
            if (integer > 0){
                return "success";
            }else{
                return "fault";
            }
        }else{
            return "fault";
        }

    }

    @Override
    public List<File> getFiles(String curPath, int curPage, int pageSize) {
        return fileMapper.getFileByPage(curPath, (curPage - 1) * pageSize, pageSize);
    }


    /**
     * 文件上传
     */
    @Override
    public String fileUpload(String pathSave, MultipartFile file) {

        // 判断文件是否存在
        Boolean fileExists = null;
        if (isDir(pathSave)){
            fileExists = hdfsWrapper.fileExists(pathSave + file.getOriginalFilename());
            if (fileExists){
                return "fault";
            }
        }else{
            fileExists = hdfsWrapper.fileExists(pathSave + file.getOriginalFilename());
            if (fileExists){
                return "fault";
            }
        }
        String uploadMsg =  hdfsWrapper.fileUpload(pathSave, file);

        if (uploadMsg != null){
            File fileInfo = hdfsWrapper.getFileInfo(pathSave + "/" + uploadMsg);
            fileMapper.insertByFile(fileInfo);
            // 如果是递归目录：查看当前上传的父目录的信息有没有存在数据库里边
            String parent = fileInfo.getParent();
            Integer level = fileInfo.getLevel();
            File fileSql = fileMapper.getFileByUrl(parent);

            while (fileSql == null && level > 1){
                // 表明这个目录不在我的数据库里边，需要插入父目录的信息
                // 从hdfs获取父目录信息
                fileInfo = hdfsWrapper.getFileInfo(parent);
                // 在数据库中插入信息
                fileMapper.insertByFile(fileInfo);

                parent = fileInfo.getParent().substring(hdfsConfig.getNameNodeUrl().length());
                level = fileInfo.getLevel();
                fileSql = fileMapper.getFileByUrl(parent);

            }
            return "success";
        }else{
            return "fault";
        }

    }


    @Override
    public String rename(String oldName, String newName) {

        if (hdfsWrapper.fileExists(newName) || !hdfsWrapper.fileExists(oldName)){
            return "fault";
        }

        boolean renameMsg = hdfsWrapper.rename(oldName, newName);
        if (renameMsg){
           // 把新路径插入表中
            File fileInfo = hdfsWrapper.getFileInfo(newName);
            Integer insertInt = fileMapper.insertByFile(fileInfo);
            //把旧路径删除
            File fileByUrl = fileMapper.getFileByUrl(oldName);
            Integer deleteFile = fileMapper.deleteFile(fileByUrl);

            //递归创建文件夹
            String parent = fileInfo.getParent();
            Integer level = fileInfo.getLevel();
            File fileSql = fileMapper.getFileByUrl(parent);

            while (fileSql == null && level > 1){
                // 表明这个目录不在我的数据库里边，需要插入父目录的信息
                // 从hdfs获取父目录信息
                fileInfo = hdfsWrapper.getFileInfo(parent);
                // 在数据库中插入信息
                fileMapper.insertByFile(fileInfo);

                parent = fileInfo.getParent();
                level = fileInfo.getLevel();
                fileSql = fileMapper.getFileByUrl(parent);
            }
            if (insertInt > 0 && deleteFile > 0){
                return "success";
            }else{
                return "fault";
            }

        }else{
            return "fault";
        }


    }

    /**
     * 文件复制到
     */
    @Override
    public String copyTo(String sourcePath, String targetpath) {
        File fileByUrl = fileMapper.getFileByUrl(sourcePath);
        if (fileByUrl == null || hdfsWrapper.fileExists(targetpath)){
            return "fault";
        }

        boolean copyMsg = hdfsWrapper.copyTo(sourcePath, targetpath);
        if (copyMsg){
            // 把新路径插入表中
            File fileInfo = hdfsWrapper.getFileInfo(targetpath);
            Integer insertInt = fileMapper.insertByFile(fileInfo);
            //递归创建文件夹
            String parent = fileInfo.getParent();
            Integer level = fileInfo.getLevel();
            File fileSql = fileMapper.getFileByUrl(parent);

            while (fileSql == null && level > 1){
                // 表明这个目录不在我的数据库里边，需要插入父目录的信息
                // 从hdfs获取父目录信息
                fileInfo = hdfsWrapper.getFileInfo(parent);
                // 在数据库中插入信息
                fileMapper.insertByFile(fileInfo);

                parent = fileInfo.getParent();
                level = fileInfo.getLevel();
                fileSql = fileMapper.getFileByUrl(parent);
            }
            if (insertInt > 0){
                return "success";
            }else{
                return "fault";
            }

        }else{
            return "fault";
        }
    }



    @Override
    public String delFile(Integer fileId) {
       // 先在表中查询,如果表中没有这个文件则直接返回
        File fileById = fileMapper.getFileByIdInBin(fileId);
        if (fileById == null){
            return "fault";
        }

        String filePath = fileById.getFilePath();

        Integer deleteFileCnt = 0;
        if (hdfsWrapper.isDir(filePath)){
            // 如果是一个文件夹，还要删除文件夹以下的所有路径
            fileMapper.deleteFileByParent(filePath);
            deleteFileCnt = fileMapper.deleteFile(fileById);
        }else{
             deleteFileCnt = fileMapper.deleteFile(fileById);
        }

        if (deleteFileCnt > 0){
            boolean delFileMsg = hdfsWrapper.delFile(filePath);
            if (delFileMsg){
                return "success";
            }else{
                return "fault";
            }
        }else{
            return "fault";
        }
    }

    @Override
    public List<File> getMusic(Integer curPage, Integer pageSize) {
        return fileMapper.getMusic((curPage - 1) * pageSize, pageSize );
    }

    @Override
    public List<File> getImg(Integer curPage, Integer pageSize) {
        return fileMapper.getImg((curPage - 1) * pageSize, pageSize );
    }

    @Override
    public List<File> getTxt(Integer curPage, Integer pageSize) {
        return fileMapper.getTxt((curPage - 1) * pageSize, pageSize);
    }

    @Override
    public List<File> getOthers(Integer curPage, Integer pageSize) {
        return fileMapper.getOthers((curPage - 1) * pageSize, pageSize);
    }

    @Override
    public List<File> getVedio(Integer curPage, Integer pageSize) {
        return fileMapper.getVedio((curPage - 1) * pageSize, pageSize);
    }

    @Override
    public File getFileInfo(String path) {
        return hdfsWrapper.getFileInfo(path);
    }

    @Override
    public FileSystem getFs() {
        return hdfsWrapper.getFs();
    }

    @Override
    public Boolean isDir(String path) {
        return hdfsWrapper.isDir(path);
    }

    @Override
    public Boolean isExist(String path) {
        return hdfsWrapper.fileExists(path);
    }

    @Override
    public Integer getTotal(String parentPath) {
        return fileMapper.getTotalCount(parentPath);
    }

    @Override
    public Integer getImgTotal() {
        return fileMapper.getImgTotal();
    }

    @Override
    public Integer getMusicTotal() {
        return fileMapper.getMusicTotal();
    }

    @Override
    public Integer getVedioTotal() {
        return fileMapper.getVedioTotal();
    }

    @Override
    public Integer getTxtTotal() {
        return fileMapper.getTxtTotal();
    }

    @Override
    public Integer getOtherTotal() {
        return fileMapper.getOthersTotal();
    }


    @Override
    public File getFileById(Integer fileId) {
        return fileMapper.getFileById(fileId);
    }

    @Override
    public List<File> getFilesByParent(String parent) {
        return fileMapper.getFileByParent(parent);
    }

    @Override
    public List<File> getDirsNotInBin(Integer curPage, Integer pageSize) {
        return fileMapper.getDirsNotInBin((curPage - 1) * pageSize, pageSize);
    }

    @Override
    public Integer getDirsTotalNotInBin() {
        return fileMapper.getDirsTotalNotinBin();
    }


}
