package edu.cuit.clouddisk.service.impl;

import edu.cuit.clouddisk.exception.FileExistsException;
import edu.cuit.clouddisk.exception.FileNotFindException;
import edu.cuit.clouddisk.mapper.FileMapper;
import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.BinService;
import edu.cuit.clouddisk.tools.wrapper.HDFSWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static edu.cuit.clouddisk.tools.utils.TimeUtils.longToDate;

/**
 * @ClassName : BinServiceImpl
 * @Author : YuYun
 * @Date : 2022-11-14 12:15:08
 * @Description ：
 */
@Service
public class BinServiceImpl implements BinService {
    @Autowired
    HDFSWrapper hdfsWrapper;

    @Autowired
    FileMapper fileMapper;

    @Override
    public Integer getTotal() {
        return fileMapper.getTotalOfBin();
    }

    @Override
    public File getFileById(Integer fileId) {
        return fileMapper.getFileOfBinById(fileId);
    }

    @Override
    public List<File> getBinFiles(Integer curPage, Integer pageSize) {
        return fileMapper.getFilesOfBin((curPage - 1) * pageSize, pageSize);
    }

    @Override
    @Transactional
    public Integer backFile(Integer fileId) {
        // 还原文件，如果是文件夹，需要批量操作, 如果不是文件夹，要判断该文件的文件夹是否在回收站，如果其父目录还在回收站则不允许还原
        File fileById = fileMapper.getFileByIdInBin(fileId);
        if (fileById == null){
            return 0;
        }

        Integer backCnt = 0;
        // 先判断他上一级有没有在垃圾桶中
        File parentFile = fileMapper.getFileByUrlInBin(fileById.getParent());
        while (parentFile != null){
            // 上一级在回收站中
            Integer fileIdOfDir = parentFile.getFileId();
            // 还原上一级
            backCnt+= fileMapper.backFileFromBin(fileIdOfDir);
            // 查询上一级的上一级
            parentFile = fileMapper.getFileByUrlInBin(parentFile.getParent());
        }


        // 如果是文件，还原他自己
        if (!hdfsWrapper.isDir(fileById.getFilePath())){
            backCnt += fileMapper.backFileFromBin(fileId);
        }
        else {
            // 如果是文件夹，还原文件夹和其中的所有文件
            backCnt +=  fileMapper.backFileFromBinParent(fileById.getFilePath());
        }
        return backCnt;
    }

    @Override
    public Integer toBinById(Integer fileId) {
        // 修改to-bin属性。文件夹需要批量操作
        File fileById = fileMapper.getFileById(fileId);
        if (fileById == null){
            return 0;
        }
        if (!hdfsWrapper.isDir(fileById.getFilePath())){
            // 如果不是文件夹
            return fileMapper.toBin(fileId, longToDate(new Date().getTime()));
        }
        return fileMapper.toBinByParent(fileById.getFilePath());
    }

}
