package edu.cuit.clouddisk.service;

import edu.cuit.clouddisk.dto.File;

import java.util.List;

/**
 * @ClassName : BinService
 * @Author : YuYun
 * @Date : 2022-11-14 12:13:24
 * @Description ：
 */
public interface BinService {


    Integer getTotal();
    /**
     * 放进回收站
     */
    Integer toBinById(Integer fileId);

    File getFileById(Integer fileId);

    /**
     * 获取回收站中的所有文件
     */
    List<File> getBinFiles(Integer curPage, Integer pageSize);

    // 根据文件ID还原文件，即修改is-tobin属性
    Integer backFile (Integer fileId);


}
