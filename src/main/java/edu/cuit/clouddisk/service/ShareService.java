package edu.cuit.clouddisk.service;

import edu.cuit.clouddisk.dto.Share;

import java.util.List;

/**
 * @ClassName : ShareService
 * @Author : YuYun
 * @Date : 2022-12-10 02:19:31
 * @Description ：
 */
public interface ShareService {
    Integer shareFile(Integer sharerId, Integer fileId, Integer userId);

    // 获取分享出去的文件信息
    List<Share> getShareOutInfo(Integer userId, Integer curPage, Integer pageSize);

    // 获取分享出去的文件的总数
    Integer getShareOutTotal(Integer sharerId, Integer curPage, Integer pageSize);

    // 获取受分享的文件信息
    List<Share> getShareInInfo(Integer sharerId, Integer curPage, Integer pageSize);

    // 获取分享出去的文件的总数
    Integer getShareInTotal(Integer sharerId);
}
