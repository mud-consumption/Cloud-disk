package edu.cuit.clouddisk.service;

import edu.cuit.clouddisk.dto.File;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName : FileService
 * @Author : YuYun
 * @Date : 2022-11-14 12:13:54
 * @Description ：
 */
public interface FileService {
    /**
     * 创建文件夹
     */

    String mkdir(String path);

    /**
     * 分页获取当前路径所有文件信息
     */
    List<File> getFiles(String curPath, int curPage, int pageSize);


    /**上传文件
     *
     */
    String fileUpload(String pathSave, MultipartFile file);

    String rename(String oldName, String newName);

    String copyTo(String sourcePath, String targetpath);

    String delFile(Integer fileId);
    /**
     * 获取音乐文件
     */

    List<File> getMusic(Integer curPage, Integer pageSize );

    /**
     * 获取图片文件
     */
    List<File> getImg(Integer curPage, Integer pageSize);

    /**
     * 获取文档文件
     */
    List<File> getTxt(Integer curPage, Integer pageSize);

    /**
     * 获取其他文件
     */
    List<File> getOthers(Integer curPage, Integer pageSize);

    /**
     * 获取视频文件
     */
    List<File> getVedio(Integer curPage, Integer pageSize);

    /**
     * 获取当前路径文件信息
     */
    File getFileInfo(String path);

    /**
     * 获取fileSystem
     */
    FileSystem getFs();

    /**
     * 是否是文件夹
     */
    Boolean isDir(String path);

    /**
     * 是否存在
     */
    Boolean isExist(String path);

    /**
     * 获取当前路径下有多少条数据
     */
    Integer getTotal(String parentPath);

    Integer getImgTotal();

    Integer getMusicTotal();

    Integer getVedioTotal();

    Integer getTxtTotal();

    Integer getOtherTotal();


    File getFileById(Integer fileId);

    List<File> getFilesByParent(String parent);

    List<File> getDirsNotInBin(Integer curPage, Integer pageSize);

    Integer getDirsTotalNotInBin();
}
