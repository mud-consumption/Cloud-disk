package edu.cuit.clouddisk.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : HdfsConfig
 * @Author : YuYun
 * @Date : 2022-11-19 21:40:36
 * @Description ：
 */
@Configuration
public class HdfsConfig {
    // hdfs nameNode连接URL
    @Value("${hdfs.nameNode.url}")
    private String nameNodeUrl;

    // 操作用户
    @Value("${hdfs.userName}")
    private String hdfsUserName;

    @Value("${hdfs.defaultSavePath}")
    private String defaultPathRoot;

    @Override
    public String toString() {
        return "HdfsConfig{" +
                "nameNodeUrl='" + nameNodeUrl + '\'' +
                ", hdfsUserName='" + hdfsUserName + '\'' +
                ", defaultPathRoot='" + defaultPathRoot + '\'' +
                '}';
    }



    public String getNameNodeUrl() {
        return nameNodeUrl;
    }

    public void setNameNodeUrl(String nameNodeUrl) {
        this.nameNodeUrl = nameNodeUrl;
    }

    public String getHdfsUserName() {
        return hdfsUserName;
    }

    public void setHdfsUserName(String hdfsUserName) {
        this.hdfsUserName = hdfsUserName;
    }

    public String getDefaultPathRoot() {
        return defaultPathRoot;
    }

    public void setDefaultPathRoot(String defaultPathRoot) {
        this.defaultPathRoot = defaultPathRoot;
    }
}
