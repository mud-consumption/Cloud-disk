package edu.cuit.clouddisk.mapper;

import edu.cuit.clouddisk.dto.Share;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @ClassName : ShareMapper
 * @Author : YuYun
 * @Date : 2022-12-10 02:20:08
 * @Description ：
 */
@Mapper
public interface ShareMapper {
    Integer insertShareInfo(Integer sharerId, Integer fileId, Integer userId, String curTimeToString);

    List<Share> getShareOutInfo(Integer userId, int startIndex, Integer pageSize);

    Integer getShareOutTotal(Integer userId);

    List<Share> getShareInInfo(Integer userId, int startIndex, Integer pageSize);

    Integer getShareInTotal(Integer userId);
}
