package edu.cuit.clouddisk.mapper;

import edu.cuit.clouddisk.dto.File;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

/**
 * @ClassName : FileMapper
 * @Author : YuYun
 * @Date : 2022-11-14 12:10:54
 * @Description ：
 */
@Mapper
public interface FileMapper {

    /**
     * 查询在回收站的总条数
     */
    @Select("select count(*) from file where is_tobin=1")
    Integer getTotalOfBin();

    /**
     * 根据ID查询回收站的文件信息
     */
    @Select("SELECT * from file where is_tobin=1 and file_id=#{fileId}")
    File getFileOfBinById(Integer fileId);

    /**
     * 查询某个路径下不在回收站的总条数
     */
    @Select("select count(*) from file where is_tobin=0 and parent=#{parentPath}")
    Integer getTotalCount(String parentPath);

    /**
     * 分页查询file表
     */
    @Select("select * from file where parent = #{curPath} and is_tobin=0 limit #{startIndex}, #{pageSize}")
    List<File> getFileByPage(String curPath, int startIndex, int pageSize);

    Integer insertByFile(@Param("file") File fileInfo);

    int update(File fileInfo);

    @Select("select * from file where file_path=#{oldUrl} and is_tobin=0")
    File getFileByUrl(@Param("oldUrl") String oldName);

    @Delete("delete from file where file_id = #{fileId}")
    Integer deleteFile(File file);

    List<File> getMusic(int startIndex, int pageSize);

    List<File> getVedio(int startIndex, int pageSize);

    List<File> getImg(int startIndex, int pageSize);

    List<File> getOthers(int startIndex, int pageSize);

    List<File> getTxt(int startIndex, int pageSize);

    @Select("select * from file where file_id = #{fileId} and is_tobin=0")
    File getFileById(Integer fileId);


    @Select("select * from file where file_id = #{fileId} and is_tobin=1")
    File getFileByIdInBin(Integer fileId);

    Integer getImgTotal();

    Integer getMusicTotal();

    Integer getVedioTotal();

    Integer getTxtTotal();

    Integer getOthersTotal();

    @Delete("delete from file where parent = #{parentPath}")
    Integer deleteFileByParent(@Param("parentPath") String parentPath);

    @Update("update file set is_tobin=1, last_modify_time=#{now} where file_id = #{fileId}")
    Integer toBin(Integer fileId, Date now);

    @Update("update file set is_tobin=1 where parent = #{dirPath} or file_path = #{dirPath}")
    Integer toBinByParent(String dirPath);

    @Select("select * from file where parent = #{parentPath}")
    List<File> getFileByParent(String parentPath);

    /**
     * 分页获取回收站中的所有文件
     */
    @Select("select * from file where is_tobin=1 limit #{startIndex}, #{pageSize}")
    List<File> getFilesOfBin(int startIndex, int pageSize);

    // 获取不在回收站中的所有文件夹
    @Select("select * from file where is_tobin=0 and file_type='Dir' limit #{startIndex}, #{pageSize}")
    List<File> getDirsNotInBin(int startIndex, int pageSize);

    // 获取不在回收站中的所有文件夹的个数
    @Select("select count(*) from file where is_tobin=0 and file_type='Dir'")
    Integer getDirsTotalNotinBin();

    // 把根据文件ID把文件的is_tobin属性修改为false
    @Update("update file set is_tobin = 0 where file_id = #{fileId} and is_tobin = 1")
    Integer backFileFromBin(Integer fileId);

    @Update("update file set is_tobin = 0 where parent=#{dirPath} or file_path=#{dirPath}")
    Integer backFileFromBinParent(String dirPath);

    @Select("select * from file where file_path = #{url} and is_tobin=1")
    File getFileByUrlInBin(String url);
}
