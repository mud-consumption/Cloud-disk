package edu.cuit.clouddisk.mapper;

import edu.cuit.clouddisk.dto.User;
import org.apache.ibatis.annotations.*;

import java.util.Date;
import java.util.List;

/**
 * @ClassName : UserMapper
 * @Author : YuYun
 * @Date : 2022-11-14 12:11:51
 * @Description ：
 */
@Mapper
public interface UserMapper {
    // 根据acc查询数据
    @Select("select * from user where acc = #{acc}")
    User getUserByacc(String acc);

    // 根据输入查询用户
    @Select("select * from user where acc = #{email} and psw = #{password}")
    User getUserByPasswordAndEmail(String password, String email);

    // 根据用户输入插入用户
    @Insert("insert into user (user_name, acc, psw) values (#{username}, #{email}, #{password})")
    Integer putUserByUsernameAndPasswordAndEmail(String username, String password, String email);

    // 获取有几条数据
    @Select("select count(*) from user where is_admin=0")
    Integer getTotalCount();

    // 分页查询
    @Select("select user_id, user_name, acc, is_admin from user  where is_admin = 0 limit #{startIndex}, #{pageSize}")
    List<User> getUserByPage(int startIndex, int pageSize);

    @Delete("delete from user where user_id = #{id}")
    Integer delUserById(int id);

    @Update("update user set is_admin = 1 where user_id = #{userId}")
    Integer modifyRole(Integer userId);

    @Update("update user set psw=#{password} where acc=#{email}")
    Integer updatePswByAcc(String email, String password);

    @Update("update user set user_name=#{username}, sex=#{sex}, phone=#{phone}, birth=#{birth} where acc=#{email}")
    Integer updateInfoByAcc(String email, String username, String sex, String phone, Date birth);

    @Select("select user_id, user_name, acc, phone from user where user_name like concat(#{username}, '%') limit #{startIndex}, #{pageSize}")
    List<User> getUserByVagueAndUserName(String username, int startIndex, int pageSize);

    @Select("select count(*) from user where user_name like concat(#{username}, '%')")
    Integer getTotalOfVague(String username);
}
