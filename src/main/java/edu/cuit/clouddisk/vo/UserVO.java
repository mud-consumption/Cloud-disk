package edu.cuit.clouddisk.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName : UserVO
 * @Author : YuYun
 * @Date : 2022-12-08 21:34:37
 * @Description ：
 */
@Data
public class UserVO implements Serializable {
    private String username;

    private String sex;

    private String phone;

    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date birth;
}
