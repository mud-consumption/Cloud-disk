package edu.cuit.clouddisk.vo;

import java.io.Serializable;

/**
 * @ClassName : UserForm
 * @Author : YuYun
 * @Date : 2022-12-03 20:43:49
 * @Description ：用于接收前端发送过来的JSON数据，主要用于登录认证
 */
public class LoginVO implements Serializable {
    private String email;
    private String password;

    public LoginVO() {
    }

    @Override
    public String toString() {
        return "UserForm{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LoginVO(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
