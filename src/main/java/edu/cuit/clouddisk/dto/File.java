package edu.cuit.clouddisk.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Component
@Api("文件实体")
public class File implements Serializable {
    @ApiModelProperty("文件主键")
    private Integer fileId;

    @ApiModelProperty("文件名称")
    private String fileName;

    @ApiModelProperty("文件大小")
    private Long fileSize;

    @ApiModelProperty("文件路径")
    private String filePath;

    @ApiModelProperty("文件类型")
    private String fileType;

    @ApiModelProperty("文件上次修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastModifyTime;
    @ApiModelProperty("父目录路径")
    private String parent;


    @ApiModelProperty("文件深度")
    private Integer level;

    @ApiModelProperty("是否被添加到了回收站")
    private Boolean isTobin;

    public File() {
    }

    public File(Integer fileId, String fileName, Long fileSize, String filePath, String fileType,
                Date lastModifyTime, String parent, Integer level, Boolean isTobin) {
        this.fileId = fileId;
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.filePath = filePath;
        this.fileType = fileType;
        this.lastModifyTime = lastModifyTime;
        this.parent = parent;
        this.level = level;
        this.isTobin = isTobin;
    }

    public Integer getFileId() {
        return fileId;
    }

    public void setFileId(Integer fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getIsTobin() {
        return isTobin;
    }

    public void setIsTobin(Boolean tobin) {
        isTobin = tobin;
    }

    @Override
    public String toString() {
        return "File{" +
                "fileId=" + fileId +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                ", filePath='" + filePath + '\'' +
                ", fileType='" + fileType + '\'' +
                ", lastModifyTime=" + lastModifyTime +
                ", parent='" + parent + '\'' +
                ", level=" + level +
                ", isTobin=" + isTobin +
                '}';
    }
}
