package edu.cuit.clouddisk.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName : ShareIn
 * @Author : YuYun
 * @Date : 2022-12-10 01:26:06
 * @Description ：
 */
@Data
public class Share implements Serializable {
    private Integer shareId;

    private User shareInUser;

    private User shareOutUser;

    private File file;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date shareTime;
}
