package edu.cuit.clouddisk.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Component
@Api("用户实体")
@Data
public class User implements Serializable {
    @ApiModelProperty("用户主键")
    private Integer userId;
    @ApiModelProperty("用户名称")
    private String userName;
    @ApiModelProperty("用户账号")
    private String acc;
    @ApiModelProperty("用户密码")
    private String psw;
    @ApiModelProperty("是否是管理员")
    private Boolean isAdmin;

    @ApiModelProperty("性别")
    private String sex;

    @ApiModelProperty("电话号码")
    private String phone;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birth;

}
