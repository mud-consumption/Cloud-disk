package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.service.AuthenService;
import edu.cuit.clouddisk.service.UserService;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : AdminController
 * @Author : YuYun
 * @Date : 2022-11-20 04:34:47
 * @Description ：
 */
@RestController
@RequestMapping("/admin")
@Slf4j
public class AdminController {
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenService authenService;

    /**
     * 用户查看
     */
    @ApiOperation("管理员用户查询")
    @GetMapping("/get_users")
    public RequestResult<?> getUsers(
            @RequestParam("curPage") int currentPage,
            @RequestParam("pageSize") int pageSize,
            @RequestHeader("token") String token){

        Boolean authenticated = authenService.authentication(token, "admin");
        if (!authenticated){
            return new RequestResult<>(403, "您没有权限访问");
        }
        List<User> users = userService.getUsers(currentPage, pageSize);
        Integer total = userService.getTotal();
        Map<String, Object> map = new HashMap<>();
        map.put("users", users);
        map.put("total", total);
        return new RequestResult<>(200, "查询成功", map);
    }


    @ApiOperation("管理员用户删除")
    @DeleteMapping("/del_users")
    public RequestResult<?> delUsers(@RequestParam(value = "userId", required = false) Integer userId,
                                     @RequestHeader("token") String token){

        if (userId == null){
            return new RequestResult<>(400, "传入参数为空");
        }
        Boolean authenticated = authenService.authentication(token, "admin");
        if (!authenticated){
            return new RequestResult<>(403, "您没有权限访问");
        }

        Integer delCnt = userService.delUserById(userId);

        if (delCnt > 0){
            return new RequestResult<>(200, "success");
        }else{
            return new RequestResult<>(400, "无此用户");
        }
    }

    @ApiOperation("提升用户权限")
    @PutMapping("/to_admin")
    public RequestResult<?> toAdmin(@RequestParam("userId") Integer userId,
                                    @RequestHeader("token") String token){
        Boolean authenticated = authenService.authentication(token, "admin");
        if (!authenticated){
            return new RequestResult<>(403, "您没有权限访问");
        }
        Integer cnt = userService.toAdmin(userId);
        if (cnt > 0){
            return new RequestResult<>(200, "success");
        }else {
            return new RequestResult<>(200, "fault");
        }
    }

    @ApiOperation("用户批量删除")
    @DeleteMapping("/delByBatch")
    public RequestResult<?> delUserByBatch(@RequestParam(value = "delList", required = false) Integer[] ids,
                                           @RequestHeader("token") String token){
        if (ids == null || ids.length == 0){
            return new RequestResult<>(400, "传入的参数为空");
        }
        Boolean authenticated = authenService.authentication(token, "admin");
        if (!authenticated){
            return new RequestResult<>(403, "您没有权限访问");
        }

        Integer delCnt = 0;
        for (Integer userId: ids){
            delCnt += userService.delUserById(userId);
        }
        if (delCnt > 0) {
            return new RequestResult<>(200 ,"success");
        }
        return new RequestResult<>(400, "fault");
    }

    @ApiOperation("批量提升用户权限")
    @PutMapping("/toAdminByBatch")
    public RequestResult<?> toAdmin(@RequestParam(value = "ids", required = false) Integer[] ids,
                                    @RequestHeader("token") String token){
        if (ids == null || ids.length == 0){
            return new RequestResult<>(400, "输入参数为空");
        }
        Boolean authenticated = authenService.authentication(token, "admin");
        if (!authenticated){
            return new RequestResult<>(403, "您没有权限访问");
        }
        for (Integer userId: ids){
            userService.toAdmin(userId);
        }
        return new RequestResult<>(200, "success");

    }



}
