package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.service.UserService;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import edu.cuit.clouddisk.tools.utils.EmailFormatVarify;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


/**
 * @ClassName : RegisterController
 * @Author : YuYun
 * @Date : 2022-11-14 12:16:38
 * @Description ：
 */

@Api("User Controller")
@RestController
@Slf4j
@CrossOrigin
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    @ApiOperation(value = "用户注册方法")
    public RequestResult<?> register(@RequestBody Map<String, String> formData) {

        String username = formData.get("username");
        String password = formData.get("password");
        String email = formData.get("email");

        if ((username == null || email == null || password == null)
        || (username.equals("") || email.equals("") || password.equals(""))){
            return new RequestResult<>(400, "输入格式不正确");
        }
        if (password.length() < 6){
            return new RequestResult<>(400, "密码不能小于6位数");
        }
        Boolean isFormatted = EmailFormatVarify.emailFormatVarify(email);
        if (!isFormatted){
            return new RequestResult<>(400, "邮箱格式不正确");
        }

        User userByAcc = userService.getUserByAcc(email);

        if (userByAcc != null){
            return new RequestResult<>(400, "用户已经存在");
        }

        Integer insert_count = userService.putUserByUsernameAndPasswordAndEmail(username,
                passwordEncoder.encode(password), email);


        if (insert_count > 0){
            return new RequestResult<>(200, "注册成功");
        }else{
            return new RequestResult<>(500, "服务器内部错误");
        }

    }

}
