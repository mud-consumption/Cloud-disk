package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.dto.Share;
import edu.cuit.clouddisk.security.TokenManager;
import edu.cuit.clouddisk.service.ShareService;
import edu.cuit.clouddisk.service.UserService;
import edu.cuit.clouddisk.tools.entity.GetRequestResult;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : ShareController
 * @Author : YuYun
 * @Date : 2022-12-10 02:22:40
 * @Description ：
 */
@RestController
@RequestMapping("/share")
@Api("分享文件Controller")
public class ShareController {

    @Autowired
    private ShareService shareService;

    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private UserService userService;

    @Autowired
    private GetRequestResult getRequestResult;

    @ApiOperation("分享文件")
    @PostMapping("/shareFile")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "fileId", value = "被分享的文件ID"),
            @ApiImplicitParam(name = "userId", value = "被分享人的ID")
    })
    public RequestResult<?> shareFile(@RequestHeader("token") String token,
            @RequestParam("fileId") Integer fileId, @RequestParam("userId") Integer userId){
        String userEmail = tokenManager.getUsernameFromToken(token);
        Integer sharerId = userService.getUserIdByAcc(userEmail);
        if (sharerId == null){
            return new RequestResult<>(415, "没有这个用户");
        }
        if (sharerId.equals(userId)){
            return new RequestResult<>(400, "不能分享给自己");
        }
        try {
            shareService.shareFile(sharerId, fileId, userId);
            return new RequestResult<>(200, "success");
        }catch (Exception e){
            e.printStackTrace();
            return new RequestResult<>(500, "服务器内部错误");
        }
    }

    @ApiOperation("分页查看个人的分享文件")
    @GetMapping("/shareOutInfo")
    public RequestResult<Map<String, Object>> getShareOutInfo(@RequestHeader("token") String token,
                                                              @RequestParam("curPage") Integer curPage,
                                                              @RequestParam("pageSize") Integer pageSize){
        String userEmail = tokenManager.getUsernameFromToken(token);
        Integer sharerId = userService.getUserIdByAcc(userEmail);
        if (sharerId == null){
            return new RequestResult<>(415, "没有这个用户");
        }
        List<Share> shares = shareService.getShareOutInfo(sharerId, curPage, pageSize);
        Integer total = shareService.getShareOutTotal(sharerId, curPage, pageSize);
        return getRequestResult.getMapRequestResult(shares, total, "shares");
    }

    @ApiOperation("分页查看接收到的分享文件")
    @GetMapping("/shareInInfo")
    public RequestResult<Map<String, Object>> getShareInInfo(@RequestHeader("token") String token,
                                                              @RequestParam("curPage") Integer curPage,
                                                              @RequestParam("pageSize") Integer pageSize){
        String userEmail = tokenManager.getUsernameFromToken(token);
        Integer sharerId = userService.getUserIdByAcc(userEmail);
        if (sharerId == null){
            return new RequestResult<>(415, "没有这个用户");
        }
        List<Share> shares = shareService.getShareInInfo(sharerId, curPage, pageSize);
        Integer total = shareService.getShareInTotal(sharerId);
        return getRequestResult.getMapRequestResult(shares, total, "shares");
    }

}
