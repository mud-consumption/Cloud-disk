package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.config.HdfsConfig;
import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.BinService;
import edu.cuit.clouddisk.service.FileService;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("/bin")
@Api("回收站相关")
@Slf4j
@RestController
public class BinController {
    @Autowired
    FileService fileService;
    @Autowired
    HdfsConfig hdfsConfig;
    @Autowired
    BinService binService;

    @ApiOperation("根据文件ID把文件放进回收站中")
    @PutMapping("/toBin")
    public RequestResult<?> toBin(@RequestParam("fileId") Integer fileId){

        // 把文件放进回收站，即修改文件表的属性，如果是文件夹，需要批量操作
        Integer toBinCnt = binService.toBinById(fileId);
        if (toBinCnt > 0){
            return new RequestResult<>(200, "删除成功");
        }else{
            return new RequestResult<>(500, "服务器内部错误");
        }
    }

    @ApiOperation("根据文件ID批量把文件放进回收站中")
    @PutMapping("/toBinByBatch")
    public RequestResult<?> toBinByBatch(@RequestParam("ids") Integer[] fileIdList) {
        int putCnt = 0;
        for (Integer fileId : fileIdList) {
            putCnt = binService.toBinById(fileId);
        }
        if (putCnt <= 0) {
            return new RequestResult<>(400, "fault");
        } else {
            return new RequestResult<>(200, "success");
        }
    }


    @ApiOperation("分页查询回收站中的文件")
    @GetMapping("/get_files")
    public RequestResult<Map<String, Object>> getFiles(@RequestParam("curPage") Integer curPage,
                                                       @RequestParam("pageSize") Integer pageSize){

        Integer total = binService.getTotal();
        Map<String, Object> map = new HashMap<>();
        map.put("total", total);
        List<File> files = binService.getBinFiles(curPage, pageSize);
        map.put("files", files);
        return new RequestResult<>(200, "查询成功", map);

    }

    @ApiOperation("还原文件")
    @PutMapping("/back_file")
    public RequestResult<?> backFile(@RequestParam(value = "fileId", required = false) Integer fileId){
        if (fileId == null){
            return new RequestResult<>(400, "传递的参数为空");
        }

        // 把 文件的is_tobin属性改为0， 但是如果是文件夹，需要把它子目录下的文件全部还原
        Integer backCnt = null;

        backCnt = binService.backFile(fileId);
        if (backCnt > 0){
            return new RequestResult<>(200, "success");
        }else{
            return new RequestResult<>(400, "fault");
        }

    }


    @ApiOperation("批量还原文件")
    @PutMapping("/backFileByBatch")
    public RequestResult<?> backFileByBatch(@RequestParam(value = "ids", required = false) Integer[] backList) {
        if (backList == null || backList.length == 0) {
            return new RequestResult<>(400, "传入的参数为空");
        }

        Integer backCnt = 0;
        for (Integer fileId : backList) {
            backCnt = binService.backFile(fileId);
        }
        if (backCnt > 0) {
            return new RequestResult<>(200, "success");
        } else {
            return new RequestResult<>(400, "fault");
        }
    }
}
