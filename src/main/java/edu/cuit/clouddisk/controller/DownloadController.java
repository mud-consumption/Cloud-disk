package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.config.HdfsConfig;
import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.FileService;
import edu.cuit.clouddisk.tools.utils.ResponseUtils;
import edu.cuit.clouddisk.tools.utils.TimeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.zip.Adler32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @ClassName : DownloadController
 * @Author : YuYun
 * @Date : 2022-11-25 08:49:46
 * @Description ：
 */

@Controller
@Api("文件下载相关")
@Slf4j
public class DownloadController {

    @Autowired
    HdfsConfig hdfsConfig;
    @Autowired
    FileService fileService;

    @ApiOperation("文件下载")
    @GetMapping("/download")
    public void fileDownLoad(
            @RequestParam("downloadPath") String downloadPath,
            HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");

        if (!fileService.isExist(downloadPath)){
            ResponseUtils.responseRender(response, 400, "文件不存在", null);
            return;
        }
        FileSystem fs = fileService.getFs();

        BufferedInputStream bis = null;
        ServletOutputStream os = null;
        File file = fileService.getFileInfo(downloadPath);
        String fileName = file.getFileName();
        try {
            response.setContentType("application/octet-stream;charset=UTF-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, StandardCharsets.UTF_8));

            os = response.getOutputStream();
            bis = new BufferedInputStream(fs.open(new Path(downloadPath)));
            FileCopyUtils.copy(bis, os);
        }catch (Exception e){
            log.error("controller: 文件下载失败" + e);
        }finally {
            // 关闭文件资源
            try {
                if (bis != null){
                    bis.close();
                }if (os != null){
                    os.flush();
                    os.close();
                }if(fs != null){
                    fs.close();
                }
            } catch (IOException e) {
                log.error("controller :文件资源关闭失败" + e);
            }
        }
    }

    @ApiOperation("文件批量下载")
    @GetMapping("/downloadByBatch")
    public void downloadByBatch(@RequestParam("downloadPaths") String[] paths,
                                HttpServletResponse response) {
        response.setCharacterEncoding("utf-8");
        ZipOutputStream zos = null;
        FileInputStream fis = null;
        BufferedInputStream buff = null;

        // 临时文件
        java.io.File zipFile = null;
        FileSystem fs = fileService.getFs();

        try {
            zipFile = java.io.File.createTempFile("test", "zip");
            FileOutputStream fot = new FileOutputStream(zipFile);
            // 为任何OutputStream产生校验，第一个参数是制定产生校验和的输出流，第二个参数是指定Checksum的类型 (Adler32(较快)和CRC32两种)
            CheckedOutputStream cos = new CheckedOutputStream(fot, new Adler32());
            zos = new ZipOutputStream(cos);
            for (String path : paths) {
                if (fs.getFileStatus(new Path(path)).isDirectory()){
                    continue;
                }

                InputStream inputStream = fs.open(new Path(path));
                File file = fileService.getFileInfo(path);
                String fileName = file.getFileName();
                if (null == inputStream) {
                    continue;
                }
                zos.putNextEntry(new ZipEntry(fileName));
                int bytesRead = 0;
                // 向压缩文件中输出数据
                while ((bytesRead = inputStream.read()) != -1) {
                    zos.write(bytesRead);
                }
                inputStream.close();
                // 当前文件写完，写入下一个文件
                zos.closeEntry();
            }
            zos.close();
            ServletOutputStream os = response.getOutputStream();
            response.setContentType("application/octet-stream;charset=UTF-8");

            String zipName = TimeUtils.curTimeToString("yyyy-MM-dd-HH:mm:ss");
            response.setHeader("Content-Disposition", "attachment;fileName="
                    + URLEncoder.encode(zipName + ".zip", StandardCharsets.UTF_8));
            response.setHeader("fileName", zipName + ".zip");
            fis = new FileInputStream(zipFile);
            buff = new BufferedInputStream(fis);
            FileCopyUtils.copy(buff, os);
        }catch (IOException e){
            e.printStackTrace();
            ResponseUtils.responseRender(response, 500, "fault", null);
        }finally {
            try {
                if (null != fis) {
                    fis.close();
                }
                if (null != buff) {
                    buff.close();
                }
                if (null != fs){
                    fs.close();
                }
            } catch (IOException e) {
                log.error( "流关闭异常");
            }
            // 删除临时文件
            if (null != zipFile) {
                zipFile.delete();
            }
        }
    }






}
