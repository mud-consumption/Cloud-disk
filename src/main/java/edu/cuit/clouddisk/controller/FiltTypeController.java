package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.FileService;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : FiltTypeController
 * @Author : YuYun
 * @Date : 2022-11-24 15:29:28
 * @Description ：
 */
@RequestMapping("/file_type")
@Api("文件类型相关")
@RestController
public class FiltTypeController {
    @Autowired
    FileService fileService;

    @GetMapping("/music")
    @ApiOperation("获取音乐类型文件")
    public RequestResult<Map<String, Object>> getMusic(@RequestParam("curPage") Integer curPage,
                                                       @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getMusic(curPage, pageSize);
        map.put("files", files);
        Integer musicTotal = fileService.getMusicTotal();
        map.put("total", musicTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

    @GetMapping("/txt")
    @ApiOperation("获取文档类型文件")
    public RequestResult<Map<String, Object>> getTXT(@RequestParam("curPage") Integer curPage,
                                                     @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getTxt(curPage, pageSize);
        map.put("files", files);

        Integer txtTotal = fileService.getTxtTotal();
        map.put("total", txtTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

    @ApiOperation("获取音频")
    @GetMapping("/vedio")
    public RequestResult<Map<String, Object>> getVedio(@RequestParam("curPage") Integer curPage,
                                                       @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getVedio(curPage, pageSize);
        map.put("files", files);

        Integer vedioTotal = fileService.getVedioTotal();
        map.put("total", vedioTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

    @ApiOperation("获取图片")
    @GetMapping("/img")
    public RequestResult<Map<String, Object>> getImg(@RequestParam("curPage") Integer curPage,
                                                     @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getImg(curPage, pageSize);

        map.put("files", files);

        Integer imgTotal = fileService.getImgTotal();
        map.put("total", imgTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

    @ApiOperation("获取其他")
    @GetMapping("/others")
    public RequestResult<Map<String, Object>> getOthers(@RequestParam("curPage") Integer curPage,
                                                        @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getOthers(curPage, pageSize);
        map.put("files", files);

        Integer otherTotal = fileService.getOtherTotal();
        map.put("total", otherTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

    @ApiOperation("获取文件夹")
    @GetMapping("/dirs")
    public RequestResult<Map<String, Object>> getDir(@RequestParam("curPage") Integer curPage,
                                                        @RequestParam("pageSize") Integer pageSize){
        Map<String, Object> map = new HashMap<>();
        List<File> files = fileService.getDirsNotInBin(curPage, pageSize);
        map.put("files", files);

        Integer otherTotal = fileService.getDirsTotalNotInBin();
        map.put("total", otherTotal);
        RequestResult<Map<String, Object>> res = new RequestResult<>(200, "查询成功");
        res.setData(map);
        return res;
    }

}
