package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.tools.entity.GetRequestResult;
import edu.cuit.clouddisk.tools.utils.StringUtils;
import edu.cuit.clouddisk.vo.UserVO;
import edu.cuit.clouddisk.dto.User;
import edu.cuit.clouddisk.security.TokenManager;
import edu.cuit.clouddisk.service.AuthenService;
import edu.cuit.clouddisk.service.UserService;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : UserController
 * @Author : YuYun
 * @Date : 2022-12-05 01:22:15
 * @Description ：
 */
@RestController
@RequestMapping("/user")
@Api
public class UserController {

    @Autowired
    private AuthenService authenService;

    @Autowired
    private TokenManager tokenManager;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private GetRequestResult getRequestResult;


    @ApiOperation("更改密码")
    @PutMapping("/updatePassword")
    public RequestResult<?> updatePsw(@RequestBody Map<String, String> map){
        if (map == null){
            return new RequestResult<>(400, "请传入参数");
        }

        String oldPassword = map.get("oldPassword");
        String newPassword = map.get("newPassword");
        String email = map.get("email");
        if (StringUtils.isEmpty(email, oldPassword, newPassword)){
            return new RequestResult<>(400, "其中一个参数为空");
        }

        if (oldPassword.equals(newPassword)){
            return new RequestResult<>(400, "旧密码和新密码相同");
        }

        User userByAcc = userService.getUserByAcc(email);
        if (userByAcc == null){
            return new RequestResult<>(412, "不存在该用户");
        }
        boolean matches = passwordEncoder.matches(oldPassword, userByAcc.getPsw());
        if (matches){
            userService.updatePswByAcc(email, passwordEncoder.encode(newPassword));
            return new RequestResult<>(200, "更新成功");
        }else {
            return new RequestResult<>(415, "密码输入错误");
        }

    }

    @ApiOperation("查看个人信息")
    @GetMapping("/getInfo")
    public RequestResult<?> getInfo(@RequestHeader("token") String token){
//        Boolean authenticated = authenService.authentication(token, "user");
//        if (!authenticated){
//            return new RequestResult<>(403, "您没有权限访问");
//        }
        String email = tokenManager.getUsernameFromToken(token);
        User userByAcc = userService.getUserByAcc(email);userByAcc.setPsw(null);
        return new RequestResult<>(200, "查询成功", userByAcc);
    }

    @ApiOperation("修改个人信息")
    @PutMapping("/updateInfo")
    public RequestResult<?> updateInfo(@RequestHeader("token") String token,
                                       @RequestBody UserVO user){
//        Boolean authenticated = authenService.authentication(token, "user");
//        if (!authenticated){
//            return new RequestResult<>(403, "您没有权限访问");
//        }
        String email = tokenManager.getUsernameFromToken(token);
        userService.updateInfo(email, user.getUsername(), user.getSex(), user.getPhone(), user.getBirth());
        return new RequestResult<>(200, "success");
    }

    @ApiOperation("模糊查询")
    @GetMapping("/getAllUserByVague")
    public RequestResult<Map<String, Object>> getAllUsers(@RequestParam("username") String username,
                                                          @RequestParam("curPage") Integer curPage,
                                                          @RequestParam("pageSize") Integer pageSize){

        List<User> users = userService.getAllUsersByVague(username, curPage, pageSize);
        Integer total = userService.getVagueTotal(username);
        return getRequestResult.getMapRequestResult(users, total, "users");
    }

}
