package edu.cuit.clouddisk.controller;

import edu.cuit.clouddisk.config.HdfsConfig;
import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.service.FileService;
import edu.cuit.clouddisk.tools.entity.GetRequestResult;
import edu.cuit.clouddisk.tools.entity.RequestResult;
import edu.cuit.clouddisk.tools.utils.PathUtils;
import edu.cuit.clouddisk.tools.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;
import java.util.Map;

@RequestMapping("/file")
@Api("文件相关")
@Slf4j
@RestController
public class FileController {

    @Autowired
    private HdfsConfig hdfsConfig;

    @Autowired
    private FileService fileService;

    @Autowired
    private GetRequestResult getRequestResult;

    @ApiOperation("文件上传")
    @PostMapping("/upload")
    public RequestResult<?> fileUpload(@RequestParam(value = "savePath", required = false)
                                                   String path,
                                       @RequestParam(value = "file")
                                               MultipartFile file){

        if (StringUtils.isEmpty(path)){
            path = hdfsConfig.getDefaultPathRoot();
        }
        path = PathUtils.standardPath(path);
        path = hdfsConfig.getNameNodeUrl() + path;
        String uploadMsg = fileService.fileUpload(path, file);
        if ("success".equals(uploadMsg)){
            return new RequestResult<>(200, uploadMsg);
        }
        else{
            return new RequestResult<>(400, uploadMsg);
        }
    }

    @ApiOperation("文件改名")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "oldName", value = "文件的旧名字"),
            @ApiImplicitParam(name = "oldPath", value = "文件的全路径(以Hdfs开头)"),
            @ApiImplicitParam(name = "newName", value = "文件的新名字")
    })
    @PutMapping("/rename")
    public RequestResult<?> fileRename(
            @RequestParam(value = "oldName", required = false) String oldName,
            @RequestParam(value = "oldPath", required = false) String oldPath,
            @RequestParam(value = "newName", required = false) String newName){

        if (StringUtils.isEmpty(oldPath) || StringUtils.isEmpty(oldName)
        || StringUtils.isEmpty(newName)){
            return new RequestResult<>(400, "传输参数为空");
        }

        String newPath = oldPath.replace(oldName, newName);

        String renameMsg = fileService.rename(oldPath, newPath);
        if (renameMsg.equals("success")){
            return new RequestResult<>(200, renameMsg);
        }else{
            return new RequestResult<>(400, renameMsg);
        }

    }

    @PutMapping("/copy_to")
    @ApiOperation("文件复制到")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "soucePath", value = "文件的旧全路径"),
            @ApiImplicitParam(name = "targetPath", value = "文件的新全路径"),
    })
    public RequestResult<?> fileCopyTo(
            @RequestParam(value = "soucePath", required = false) String sourcePath,
            @RequestParam(value = "targetPath", required = false) String targetpath){
        if (StringUtils.isEmpty(sourcePath) || StringUtils.isEmpty(targetpath)){
            return new RequestResult<>(400, "输入参数为空");
        }

        String copyMsg = fileService.copyTo(sourcePath, targetpath);
        if("success".equals(copyMsg)){
            return new RequestResult<>(200, copyMsg);
        }else{
            return new RequestResult<>(400, copyMsg);
        }
    }

    @PutMapping("/move_to")
    @ApiOperation("文件移动到")
    @ApiImplicitParams(value = {
            @ApiImplicitParam(name = "oldPath", value = "文件的旧全路径"),
            @ApiImplicitParam(name = "newPath", value = "文件的新全路径"),
    })
    public RequestResult<?> fileMoveTo(
            @RequestParam(value = "oldPath", required = false) String oldPath,
            @RequestParam(value = "newPath", required = false) String newPath){

        if (StringUtils.isEmpty(oldPath) || StringUtils.isEmpty(newPath)){
            return new RequestResult<>(400, "传输参数为空");
        }

        String renameMsg = fileService.rename(oldPath, newPath);
        if (renameMsg.equals("success")){
            return new RequestResult<>(200, renameMsg);
        }else{
            return new RequestResult<>(400, renameMsg);
        }

    }

    @ApiOperation("创建文件夹")
    @PostMapping("/mkdir")
    public RequestResult<?> mkdir(@RequestParam("dirpath") String dirpath){
        dirpath = PathUtils.standardPath(dirpath);
        dirpath = hdfsConfig.getNameNodeUrl()+dirpath;
        String isMkdir = fileService.mkdir(dirpath);
        if ("success".equals(isMkdir)){
            return new RequestResult<>(200, isMkdir);
        }
        else{
            return new RequestResult<>(400, isMkdir);
        }
    }

    @ApiOperation("查询当前路径所有文件信息")
    @GetMapping("/file_query")
    public RequestResult<Map<String, Object>>  fileQuery(
            @RequestParam("curPage") int curPage,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "curPath", required = false) String curPath){

        if(StringUtils.isEmpty(curPath)){
            curPath = hdfsConfig.getDefaultPathRoot();
        }
        curPath = PathUtils.standardPath(curPath);
        curPath = hdfsConfig.getNameNodeUrl()+curPath;
        List<File> files = fileService.getFiles(curPath, curPage, pageSize);
        Integer total = fileService.getTotal(curPath);
        return getRequestResult.getMapRequestResult(files, total, "files");
    }


    @ApiOperation("删除文件")
    @DeleteMapping("/delete")
    public RequestResult<?> deleteFile(@RequestParam("fileId") Integer fileID){

        String delMsg = fileService.delFile(fileID);
        if (delMsg.equals("success")){
            return new RequestResult<>(200, delMsg);
        }else{
            return new RequestResult<>(400, delMsg);
        }
    }

    @ApiOperation("批量删除文件")
    @DeleteMapping("/deleteByBatch")
    public RequestResult<?> deleteFileByBatch(@RequestParam("delList") Integer[] delList){
        int delCnt = 0;
        for (Integer fileId : delList){
            String delMsg = fileService.delFile(fileId);
            if (delMsg.equals("success")){
                delCnt += 1;
            }
        }
        if (delCnt > 0){
            return new RequestResult<>(200, "success");
        }else{
            return new RequestResult<>(400, "fault");
        }
    }

    @ApiOperation("模糊查询文件")
    @GetMapping("/fileQueryByVague")
    public RequestResult<Map<String, Object>> getFileByVague(@RequestParam("curPage") Integer curPage,
                                                             @RequestParam("pageSize") Integer pageSize,
                                                             @RequestParam("fileName") String fileName){
        return null;
    }

}