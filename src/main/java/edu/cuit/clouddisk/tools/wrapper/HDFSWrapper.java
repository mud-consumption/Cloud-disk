package edu.cuit.clouddisk.tools.wrapper;

import edu.cuit.clouddisk.dto.File;
import edu.cuit.clouddisk.tools.utils.HdfsUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @ClassName : HDFSWrapper
 * @Author : YuYun
 * @Date : 2022-11-20 06:59:38
 * @Description ：
 */
@Component
@Slf4j
public class HDFSWrapper {
    @Autowired
    private HdfsUtils hdfsUtils;

    /**
     * 创建文件夹
     */
    public boolean mkdir(String path){
         return hdfsUtils.mkdir(path);
    }

    /**
     * 查询文件信息
     */

    public File getFileInfo(String path){
        return hdfsUtils.getSingleFileInfo(path);
    }

    /**
     * 文件是否存在
     */
    public boolean fileExists(String path){
        return hdfsUtils.existFile(path);
    }

    /**
     * 文件上传
     */
    public String fileUpload(String pathSave, MultipartFile file){
        return hdfsUtils.uploadFile(pathSave, file);
    }

    public boolean rename(String oldName, String newName) {
       return hdfsUtils.renameFile(oldName, newName);
    }

    public boolean copyTo(String sourcePath, String targetpath) {
        return hdfsUtils.copyFileTo(sourcePath, targetpath) ;
    }

    public String getContent(String filePath) {
        return hdfsUtils.readFile(filePath);
    }

    public boolean delFile(String filePath) {
        return  hdfsUtils.deleteFile(filePath);
    }


    public FileSystem getFs() {
        try {
            return hdfsUtils.getFileSystem();
        }catch (Exception e){
            log.error("wrapper: 获取fs失败" + e);
        }
        return null;
    }

    public Boolean isDir(String path) {
        return hdfsUtils.isDir(path);
    }
}
