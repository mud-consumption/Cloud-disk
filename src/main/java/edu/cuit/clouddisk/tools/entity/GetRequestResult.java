package edu.cuit.clouddisk.tools.entity;

import edu.cuit.clouddisk.dto.Share;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : GetRequetResult
 * @Author : YuYun
 * @Date : 2022-12-10 03:15:23
 * @Description ：
 */
@Component
public class GetRequestResult {
    public <T> RequestResult<Map<String, Object>> getMapRequestResult(List<T> shares, Integer total, String listName) {
        RequestResult<Map<String, Object>> res = new RequestResult<>();
        Map<String, Object> map = new HashMap<>();
        map.put(listName, shares);
        map.put("total", total);
        res.setData(map);
        res.setMsg("success");
        res.setCode(200);
        return res;
    }
}
