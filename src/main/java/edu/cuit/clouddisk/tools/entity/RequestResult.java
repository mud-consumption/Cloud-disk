package edu.cuit.clouddisk.tools.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import edu.cuit.clouddisk.dto.Share;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestResult<T>{

    private Integer code;

    private String msg;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private long count;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;

    public RequestResult(Integer code, String msg, long count, T data) {
        this.code = code;
        this.msg = msg;
        this.count = count;
        this.data = data;
    }

    public RequestResult() {
    }


    public RequestResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


    public RequestResult(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "RequestResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", count=" + count +
                ", data=" + data +
                '}';
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
