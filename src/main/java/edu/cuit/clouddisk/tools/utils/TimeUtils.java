package edu.cuit.clouddisk.tools.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName : TimeUtils
 * @Author : YuYun
 * @Date : 2022-11-20 06:00:35
 * @Description ：
 */
public class TimeUtils {
    public static String curTimeToString(){
        Date date = new Date();//获取当前的日期
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        return df.format(date);
    }

    public static String curTimeToString(String format){
        Date date = new Date();//获取当前的日期
        SimpleDateFormat df = new SimpleDateFormat(format);//设置日期格式
        return df.format(date);
    }
    // 将long类型转为时间戳
    public static Date longToDate(long lo) {
        return new Date(lo);
    }

}
