package edu.cuit.clouddisk.tools.utils;

/**
 * @ClassName : PathUtils
 * @Author : YuYun
 * @Date : 2022-11-22 02:34:24
 * @Description ：
 */
public class PathUtils {
    public static String standardPath(String path){
        if (!path.startsWith("/")){
            return "/" + path;
        }else{
            return path;
        }
    }
}
