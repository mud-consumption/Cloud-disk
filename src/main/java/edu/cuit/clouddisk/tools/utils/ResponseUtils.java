package edu.cuit.clouddisk.tools.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.cuit.clouddisk.tools.entity.RequestResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ClassName : ResponseUtils
 * @Author : YuYun
 * @Date : 2022-12-06 18:03:26
 * @Description ：
 */
public class ResponseUtils {
    private final static ObjectMapper objectMapper = new ObjectMapper();

    public static void responseRender(HttpServletResponse response, Integer code, String msg, Object data) {
        PrintWriter writer = null;
        try {
            response.setContentType("application/json;charset=utf-8");
            response.setStatus(200);
            writer = response.getWriter();
            RequestResult<Object> res = new RequestResult<>(code, msg, data);
            writer.write(objectMapper.writeValueAsString(res));
            writer.flush();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if (writer != null){
                writer.close();
            }
        }

    }
}
