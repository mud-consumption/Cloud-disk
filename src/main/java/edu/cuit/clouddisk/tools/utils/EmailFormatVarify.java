package edu.cuit.clouddisk.tools.utils;

public class EmailFormatVarify {
    public static Boolean emailFormatVarify(String email){
        String regex = "[1-9][0-9]{8,10}[@][q][q][.][c][o][m]";
        return email.matches(regex);
    }
}
