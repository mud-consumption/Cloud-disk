package edu.cuit.clouddisk.tools.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.FileSystem;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @ClassName : ResourceClose
 * @Author : YuYun
 * @Date : 2022-11-19 22:26:04
 * @Description ：
 */
@Slf4j
public class ResourceClose {
    public static void resourceClose(InputStream inputStream,
                                     OutputStream outputStream, FileSystem fs) {
        try {
            if (inputStream != null){
                inputStream.close();
            }
            if (outputStream != null){
                outputStream.close();
            }
            if (fs != null) {
                fs.close();
            }
        }catch (Exception e){
            log.error("关闭资源错误：" + e);
        }

    }

    public static void resourceClose(InputStream inputStream, OutputStream outputStream) {
        try {
            if (inputStream != null){
                inputStream.close();
            }
            if (outputStream != null){
                outputStream.close();
            }
        }catch (Exception e){
            log.error("关闭资源错误：" + e);
        }

    }

    public static void resourceClose(FileSystem fs) {
        try {
            if (fs != null){
                fs.close();
            }
        }catch (Exception e){
            log.error("关闭资源错误：" + e);
        }

    }

    public static void resourceClose(InputStream inputStream, FileSystem fileSystem){
        try {
            if(fileSystem!=null){
                fileSystem.close();
            }if(inputStream != null){
                inputStream.close();
            }
        }catch (Exception e){
            log.error("关闭资源错误：" + e);
        }
    }
}
