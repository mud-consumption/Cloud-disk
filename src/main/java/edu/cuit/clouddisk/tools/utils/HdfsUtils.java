package edu.cuit.clouddisk.tools.utils;

import edu.cuit.clouddisk.config.HdfsConfig;
import edu.cuit.clouddisk.dto.File;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static org.eclipse.jetty.util.IO.bufferSize;

/**
 * @ClassName : HdfsUtils
 * @Author : YuYun
 * @Date : 2022-11-19 21:45:29
 * @Description ：
 */

@Component
@Slf4j
public class HdfsUtils {

    @Autowired
    private HdfsConfig hdfsConfig;

    /**
     * hdf 配置
     */
    private Configuration getConfiguration() {
        Configuration configuration = new Configuration();
        configuration.set("dfs.replication", "1");
        return configuration;
    }


    /**
     * 获取HDFS文件系统对象
     */
    public FileSystem getFileSystem() throws URISyntaxException, IOException, InterruptedException {
        return FileSystem.get(new URI(hdfsConfig.getNameNodeUrl()), getConfiguration(), hdfsConfig.getHdfsUserName());
    }

    /**
     * 判断HDFS文件是否存在
     */
    public boolean existFile(String path) {
        boolean isExists = false;
        if (StringUtils.isEmpty(path)) {
            return false;
        }
        try (FileSystem fs = getFileSystem()) {
            Path srcPath = new Path(path);
            isExists = fs.exists(srcPath);
        } catch (Exception e) {
            log.info("existFile " + e);
        }
        return isExists;
    }

    /**
     * 创建文件夹
     */
    public boolean mkdir(String path) {
        FileSystem fs = null;
        boolean isOk = false;
        if (StringUtils.isEmpty(path)) {
            return false;
        }
        try {
            if (existFile(path)) {
                log.info("hdfs file is exists: " + path);
                return false;
            }
            // 目标路径

            fs = getFileSystem();
            Path srcPath = new Path(path);
            isOk = fs.mkdirs(srcPath);
            log.info("hdfs mkdir success: " + path);
        } catch (Exception e) {
            log.info("hdfs mkdir: " + e);
        } finally {
            ResourceClose.resourceClose(fs);
        }
        return isOk;
    }

    /**
     * HDFS上传文件
     */
    public String uploadFile(String path, MultipartFile file) {
        if (file == null || StringUtils.isEmpty(path)) {
            log.info("文件或路径不存在");
            return null;
        }

        FileSystem fs = null;
        FSDataOutputStream outputStream = null;
        InputStream inputStream = null;

        try {
            fs = getFileSystem();
            String fileName = file.getOriginalFilename();
            Path newPath = new Path(path + "/" + fileName);
            outputStream = fs.create(newPath);
            inputStream = new BufferedInputStream(file.getInputStream());
            IOUtils.copyBytes(inputStream, outputStream, bufferSize);
            return fileName;
        }
        catch (Exception e){
            log.info("上传时异常" + e);
            return null;
        }finally {
            ResourceClose.resourceClose(inputStream, outputStream, fs);
        }
    }


    /**
     * HDFS重命名文件， 移动文件
     */
    public boolean renameFile(String oldName, String newName) {
        if (StringUtils.isEmpty(oldName) || StringUtils.isEmpty(newName)) {
            return false;
        }
        if (!existFile(oldName)){
            return false;
        }
        FileSystem fs = null;
        boolean isOk = false;
        try {

            fs = getFileSystem();
            // 原文件目标路径
            Path oldPath = new Path(oldName);
            // 重命名目标路径
            Path newPath = new Path(newName);
            isOk = fs.rename(oldPath, newPath);

            return isOk;
        } catch (Exception e) {
            log.error("移动文件失败：" + e);
        } finally {
            ResourceClose.resourceClose(fs);
        }
        return isOk;
    }

    /**
     * 删除HDFS文件
     *
     */
    public boolean deleteFile(String path) {

        if (StringUtils.isEmpty(path)) {
            log.info("文件不存在");
            return false;
        }
        FileSystem fs = null;
        boolean isOk = false;
        try {
            if (!existFile(path)) {
                return false;
            }
            fs = getFileSystem();
            Path srcPath = new Path(path);
            isOk = fs.deleteOnExit(srcPath);
        } catch (Exception e) {
            log.error("hdfs deleteFile " + e);
        } finally {
           ResourceClose.resourceClose(fs);
        }
        return isOk;
    }


    /**
     * HDFS文件复制到
     */
    public boolean copyFileTo(String sourcePath, String targetPath) {
        if (StringUtils.isEmpty(sourcePath) || StringUtils.isEmpty(targetPath)) {
            return false;
        }

        // 原始文件路径
        Path oldPath = new Path(sourcePath);
        if (!existFile(sourcePath)){
            return false;
        }
        // 目标路径
        Path newPath = new Path(targetPath);

        FSDataInputStream inputStream = null;
        FSDataOutputStream outputStream = null;
        FileSystem fs = null;
        try {

            fs = getFileSystem();
            inputStream = fs.open(oldPath);
            outputStream = fs.create(newPath);

            IOUtils.copyBytes(inputStream, outputStream, bufferSize, false);
        } catch (Exception e){
            log.error("文件复制失败!" + e);
        }finally {
            ResourceClose.resourceClose(inputStream, outputStream, fs);
        }
        return true;
    }

    /**
     * 读取HDFS目录信息
     *
     */
    public File getSingleFileInfo(String path) {
        if (!existFile(path)){
            return null;
        }
        FileSystem fs = null;

        try {
            fs = getFileSystem();
            FileStatus fileStatus = fs.getFileStatus(new Path(path));
            String fileName = fileStatus.getPath().getName();
            String filePath = fileStatus.getPath().toString();
            long fileSize = fileStatus.getLen();
            fileSize = fileSize/1024; // 把单位转化成KB
            String fileType = null;
            if (fileStatus.isFile()){
                fileType = fileName.substring(fileName.lastIndexOf('.')+1);
                fileType = fileType.toLowerCase();
            }else{
                fileType = "Dir";
            }

            Date modifyTime = TimeUtils.longToDate(fileStatus.getModificationTime());
            Path parent = fileStatus.getPath().getParent();
            String parentPath = null;
            if (parent != null){
                parentPath = parent.toString();
            }
            int level = fileStatus.getPath().depth();

            return new File(null,fileName,fileSize, filePath, fileType,
                    modifyTime,parentPath,level, null);

        } catch (Exception e){
            log.error("文件信息获取失败：" + e);
            return null;
        }finally {
            ResourceClose.resourceClose(fs);
        }

    }

    /**
     * 读取HDFS文件内容
     *
     */
    public String readFile(String path) {
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        if (!existFile(path)) {
            return null;
        }
        FileSystem fs = null;
        FSDataInputStream inputStream = null;
        try {
            // 目标路径
            Path srcPath = new Path(path);
            fs = getFileSystem();
            inputStream = fs.open(srcPath);
            // 防止中文乱码
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String lineTxt = "";
            StringBuilder sb = new StringBuilder();
            while ((lineTxt = reader.readLine()) != null) {
                sb.append(lineTxt);
            }
            return sb.toString();
        } catch (Exception e){
            log.error("获取文件内容失败!" + e);
        }finally {
            ResourceClose.resourceClose(inputStream, fs);
        }
        return null;
    }

    public Boolean isDir(String path) {
        if (!existFile(path)){
            return false;
        }
        FileSystem fs = null;
        try{
            fs = getFileSystem();
            return fs.getFileStatus(new Path(path)).isDirectory();
        }catch (Exception e){
            log.error("查询文件类型失败" + e);
        }finally {
            ResourceClose.resourceClose(fs);
        }
        return null;
    }
}