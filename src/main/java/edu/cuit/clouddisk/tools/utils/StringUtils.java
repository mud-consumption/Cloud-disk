package edu.cuit.clouddisk.tools.utils;

/**
 * @ClassName : StringUtils
 * @Author : YuYun
 * @Date : 2022-11-19 21:58:46
 * @Description ：
 */
public class StringUtils {
    public static boolean isEmpty(String str){
        return str == null || str.equals("");
    }

    public static boolean isEmpty(String... strs){
        if (strs.length == 0){
            return true;
        }
        for (String str: strs){
            if (str.isEmpty()){
                return true;
            }
        }
        return false;
    }
}
