package edu.cuit.clouddisk.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.cuit.clouddisk.vo.LoginVO;
import edu.cuit.clouddisk.security.SecurityUser;
import edu.cuit.clouddisk.security.TokenManager;
import edu.cuit.clouddisk.tools.utils.ResponseUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName : TokenLoginFilter
 * @Author : YuYun
 * @Date : 2022-12-03 18:06:50
 * @Description ：
 */

public class TokenLoginFilter extends UsernamePasswordAuthenticationFilter {
    // 登录认证过滤器

    private final TokenManager tokenManager;
    private final RedisTemplate<String, Object> redisTemplate;
    private final AuthenticationManager authenticationManager;

    public TokenLoginFilter(AuthenticationManager authenticationManager,
                            TokenManager tokenManager,
                            RedisTemplate<String, Object> redisTemplate) {
        this.authenticationManager = authenticationManager;
        this.tokenManager = tokenManager;
        this.redisTemplate = redisTemplate;
        // 设置允许其他请求
        this.setPostOnly(false);
        this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        ObjectMapper objectMapper = new ObjectMapper();
        Authentication authenticate = null;
        try {
            LoginVO user = objectMapper.readValue(request.getInputStream(), LoginVO.class);
            // Security 校验
             authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(),
                    user.getPassword(),
                    new ArrayList<>()));
        } catch (IOException e) {
            e.printStackTrace();
            ResponseUtils.responseRender(response, 500, "服务器异常或输入格式错误", null);
        }
        return authenticate;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult) throws IOException,
            ServletException {
        SecurityUser securityUser = (SecurityUser)authResult.getPrincipal();
        String username = securityUser.getUsername();
        // 生成Token
        String token = tokenManager.createToken(username);
        long exp = tokenManager.getExp(token);
        // 存入Redis
        redisTemplate.opsForValue().set(username, securityUser.getPermissionValueList(), exp,
                TimeUnit.MILLISECONDS);
        // 返回Token

        HashMap<String, Object> map = new HashMap<>();
        map.put("token", token);
        ResponseUtils.responseRender(response, 200, "登录成功", map);

    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException,
            ServletException {
        ResponseUtils.responseRender(response, 400, "认证失败", null);
    }
}
