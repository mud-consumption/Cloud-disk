package edu.cuit.clouddisk.filter;

import edu.cuit.clouddisk.security.TokenManager;
import edu.cuit.clouddisk.tools.utils.ResponseUtils;
import edu.cuit.clouddisk.tools.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.util.CollectionUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @ClassName : TokenAuthFilter
 * @Author : YuYun
 * @Date : 2022-12-03 18:42:07
 * @Description ：
 */
@Slf4j
public class TokenAuthFilter extends BasicAuthenticationFilter {

    private final TokenManager tokenManager;

    private final RedisTemplate<String, Object> redisTemplate;


    public TokenAuthFilter(TokenManager tokenManager,
                           RedisTemplate<String, Object> redisTemplate,
                           AuthenticationManager authenticationManager) {
        super(authenticationManager);
        this.redisTemplate = redisTemplate;
        this.tokenManager = tokenManager;
    }


    // 携带token进行操作
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        // 获取TOken
        String token = request.getHeader("token");
        if (StringUtils.isEmpty(token)){
            tokenEmptyResponse(response);
            return;
        }


        String username = tokenManager.getUsernameFromToken(token);
        // 从redis中获取权限
        Object authenOfRedis = null;
        try {
            authenOfRedis = redisTemplate.opsForValue().get(username);
        }catch (IllegalArgumentException e){
            log.info("redis exception: " + e.getMessage());
        }

        if (authenOfRedis == null){
            onUnsuccessfulAuthentication(request, response, null);
            return;
        }

        List<String> permissionValuelist = (List<String>) redisTemplate.opsForValue().get(username);

        // 存放权限
        Collection<GrantedAuthority> authorityCollection = new ArrayList<>();
        // 将权限存入到权限上下文中，表示当前用户具备哪些权限
        if (!CollectionUtils.isEmpty(permissionValuelist)){
            for (String permissionValue : permissionValuelist) {
                SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permissionValue);
                authorityCollection.add(authority);
            }
        }
        // 生成权限信息对象
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, token, authorityCollection);
        // 存到权限上下文中
        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        // 放行
        chain.doFilter(request, response);

    }

    @Override
    protected void onUnsuccessfulAuthentication(HttpServletRequest request,
                                                HttpServletResponse response, AuthenticationException failed)
            throws IOException {
            ResponseUtils.responseRender(response, 403, "登录信息已过期", null);
    }

    protected void tokenEmptyResponse(HttpServletResponse response) throws IOException{
        ResponseUtils.responseRender(response, 403, "未携带用户信息", null);
    }
}
