package edu.cuit.clouddisk.exception;

/**
 * @ClassName : FileNotFindException
 * @Author : YuYun
 * @Date : 2022-11-25 12:23:28
 * @Description ：
 */
public class FileNotFindException extends Exception{
    public FileNotFindException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public FileNotFindException(String msg) {
        super(msg);
    }
}
