package edu.cuit.clouddisk.exception;

/**
 * @ClassName : FileExistsException
 * @Author : YuYun
 * @Date : 2022-12-08 01:06:34
 * @Description ：
 */
public class FileExistsException extends Exception{
    public FileExistsException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public FileExistsException(String msg) {
        super(msg);
    }
}
