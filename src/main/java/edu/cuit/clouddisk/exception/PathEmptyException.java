package edu.cuit.clouddisk.exception;

/**
 * @ClassName : PathEmptyException
 * @Author : YuYun
 * @Date : 2022-11-25 12:21:56
 * @Description ：
 */
public class PathEmptyException extends Exception{
    public PathEmptyException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public PathEmptyException(String msg) {
        super(msg);
    }
}
