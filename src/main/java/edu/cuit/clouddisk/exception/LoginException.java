package edu.cuit.clouddisk.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @ClassName : LoginException
 * @Author : YuYun
 * @Date : 2022-11-15 10:13:00
 * @Description ：
 */
public class LoginException extends AuthenticationException {

    public LoginException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public LoginException(String msg) {
        super(msg);
    }


}
