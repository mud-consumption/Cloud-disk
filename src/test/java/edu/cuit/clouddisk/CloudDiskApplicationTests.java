package edu.cuit.clouddisk;

import edu.cuit.clouddisk.security.TokenManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class CloudDiskApplicationTests {

    @Autowired
    TokenManager tokenManager;
    @Test
    void contextLoads() {
        String email = "3292418225@qq.com";
        String token = tokenManager.createToken(email);
        assert tokenManager.getUsernameFromToken(token).equals(email);
        System.out.println(tokenManager.getUsernameFromToken("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIyMzUyNDAyNDM4QHFxLmNvbSIsImV4cCI6MTY3MDA3OTg5OX0.AZ99Qh0nTp3usdGq1s22Z0ZIiAAkxutHMcyYUdPhmDA"));
    }

}
