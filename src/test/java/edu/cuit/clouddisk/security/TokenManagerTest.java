package edu.cuit.clouddisk.security;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @ClassName : TokenManagerTest
 * @Author : YuYun
 * @Date : 2022-12-03 22:08:11
 * @Description ：
 */
@SpringBootTest
public class TokenManagerTest {
    @Autowired
    TokenManager tokenManager;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Test
    public void tokenManagerTest(){
        String email = "3292418225@qq.com";
        String token = tokenManager.createToken(email);
        assert tokenManager.getUsernameFromToken(token).equals(email);
    }

    @Test
    public void passwordTest(){
        boolean matches = passwordEncoder.matches("yuyun123", "$2a$10$LufYIHsftT7jd6I12rcTAOcrO5W6Pii4y/ijedNL8Nj0Bhu/IIoPC");
        assert matches;
    }
}
