package edu.cuit.clouddisk.hdfs;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * @ClassName : HdfsUtilsTest
 * @Author : YuYun
 * @Date : 2022-12-06 16:21:39
 * @Description ：
 */
@Slf4j
@SpringBootTest
public class HdfsUtilsTest {
    @Test
    public void getFileInfo() throws URISyntaxException, IOException, InterruptedException {
        URI uri = new URI("hdfs://hadoop102:8020");
        Configuration entries = new Configuration();
        entries.set("dfs.replication", "1");

        String user = "yuyun";
        FileSystem fs = FileSystem.get(uri, entries,user);
        FileStatus fileStatus = fs.getFileStatus(new Path("hdfs://hadoop102:8020/"));
        assert fileStatus != null;

    }

}
