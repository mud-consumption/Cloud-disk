/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : clouddisk

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 10/12/2022 03:53:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for file
-- ----------------------------
DROP TABLE IF EXISTS `file`;
CREATE TABLE `file`  (
  `file_id` int(0) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件名',
  `file_size` bigint(0) NOT NULL COMMENT '文件大小',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件路径',
  `file_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文件类型',
  `last_modify_time` timestamp(0) NOT NULL COMMENT '上次修改时间',
  `parent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父节点',
  `level` smallint(0) NOT NULL COMMENT '目录深度',
  `is_tobin` tinyint(0) NULL DEFAULT 0,
  PRIMARY KEY (`file_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 87 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file
-- ----------------------------
INSERT INTO `file` VALUES (74, 'newDir01', 0, 'hdfs://hadoop102:8020/newDir01', 'Dir', '2022-12-08 01:20:12', 'hdfs://hadoop102:8020/', 1, 0);
INSERT INTO `file` VALUES (75, 'newDir02', 0, 'hdfs://hadoop102:8020/newDir02', 'Dir', '2022-12-08 01:20:16', 'hdfs://hadoop102:8020/', 1, 0);
INSERT INTO `file` VALUES (76, 'newDir03', 0, 'hdfs://hadoop102:8020/newDir03', 'Dir', '2022-12-08 01:20:20', 'hdfs://hadoop102:8020/', 1, 0);
INSERT INTO `file` VALUES (77, 'newDir04', 0, 'hdfs://hadoop102:8020/newDir04', 'Dir', '2022-12-08 01:20:23', 'hdfs://hadoop102:8020/', 1, 0);
INSERT INTO `file` VALUES (78, 'LDA数学八卦.pdf', 2113, 'hdfs://hadoop102:8020/newDir01/LDA数学八卦.pdf', 'pdf', '2022-12-08 01:54:15', 'hdfs://hadoop102:8020/newDir01', 2, 0);
INSERT INTO `file` VALUES (79, 'MAGIC.pdf', 9057, 'hdfs://hadoop102:8020/newDir01/MAGIC.pdf', 'pdf', '2022-12-08 01:54:39', 'hdfs://hadoop102:8020/newDir01', 2, 0);
INSERT INTO `file` VALUES (80, 'test01', 0, 'hdfs://hadoop102:8020/newDir04/test01', 'Dir', '2022-12-08 01:24:09', 'hdfs://hadoop102:8020/newDir04', 2, 0);
INSERT INTO `file` VALUES (81, 'test01', 0, 'hdfs://hadoop102:8020/newDir01/test01', 'Dir', '2022-12-08 01:24:21', 'hdfs://hadoop102:8020/newDir01', 2, 0);
INSERT INTO `file` VALUES (82, 'test02', 0, 'hdfs://hadoop102:8020/newDir01/test02', 'Dir', '2022-12-08 01:24:26', 'hdfs://hadoop102:8020/newDir01', 2, 0);
INSERT INTO `file` VALUES (83, 'test02', 0, 'hdfs://hadoop102:8020/newDir02/test02', 'Dir', '2022-12-08 01:24:41', 'hdfs://hadoop102:8020/newDir02', 2, 0);
INSERT INTO `file` VALUES (84, 'test01', 0, 'hdfs://hadoop102:8020/newDir02/test01', 'Dir', '2022-12-08 01:24:44', 'hdfs://hadoop102:8020/newDir02', 2, 0);
INSERT INTO `file` VALUES (85, 'Portrait.pdf', 4823, 'hdfs://hadoop102:8020/newDir03/Portrait.pdf', 'pdf', '2022-12-08 01:25:05', 'hdfs://hadoop102:8020/newDir03', 2, 0);
INSERT INTO `file` VALUES (86, 'Portrait.pdf', 4823, 'hdfs://hadoop102:8020/newDir04/Portrait.pdf', 'pdf', '2022-12-09 17:35:32', 'hdfs://hadoop102:8020/newDir04', 2, 0);

-- ----------------------------
-- Table structure for share
-- ----------------------------
DROP TABLE IF EXISTS `share`;
CREATE TABLE `share`  (
  `share_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '分享表主键',
  `file_id` int(0) NULL DEFAULT NULL COMMENT '分享进来的文件',
  `share_in_user` int(0) NULL DEFAULT NULL COMMENT '被分享的人',
  `share_time` timestamp(0) NULL DEFAULT NULL COMMENT '分享时间',
  `share_out_user` int(0) NULL DEFAULT NULL COMMENT '把文件分享出去的人',
  PRIMARY KEY (`share_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of share
-- ----------------------------
INSERT INTO `share` VALUES (1, 78, 17, '2022-12-10 03:32:56', 5);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(0) NOT NULL AUTO_INCREMENT COMMENT '用户主键',
  `user_name` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `acc` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户账号',
  `psw` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户密码',
  `is_admin` tinyint(0) NOT NULL DEFAULT 0 COMMENT '是否是管理员',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'male' COMMENT '性别',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '123' COMMENT '电话号码',
  `birth` date NULL DEFAULT '1999-12-31' COMMENT '生日',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `acc`(`acc`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (5, 'yuyun', 'xxxxxxxxx@qq.com', '$2a$10$c2dDLZQBOf9tMhim8lpxIu5YCz2fbbSowNU2bv8l0j3vi9kP2q7Zm', 1, NULL, NULL, NULL);
INSERT INTO `user` VALUES (15, 'wxy', 'xxxxxxxx@qq.com', '$2a$10$TYarGHy/pxgaR9qtLAgSFORj5uxBNmKcrGDS0XOKqms9ffUxAYXt.', 0, '男', 'xxxxxxxx', '2018-02-12');
INSERT INTO `user` VALUES (17, 'xjr', 'xxxxxxx@qq.com', '$2a$10$dlzzHXF53XwFQJlotkudMOoP1KdZJRhhQkO4Jkjn9LEq9ESqNKYwS', 0, '女', '123', '1999-12-31');

SET FOREIGN_KEY_CHECKS = 1;
